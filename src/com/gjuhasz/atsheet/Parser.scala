package com.gjuhasz.atsheet

import org.apache.poi.ss.usermodel.Row
import scala.reflect.ClassTag
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import com.gjuhasz.legacy.SheetField
import java.io.InputStream
import org.apache.poi.ss.{ usermodel => poi }

import scala.reflect.runtime.{ universe => ru }

class Parser(is: InputStream) {

  def parseTo[TargetT: ru.TypeTag]() = {

  }

  def parse() = {
    val workbook = new HSSFWorkbook(is)
    parseWorkBook(workbook)
  }

  def parseWorkBook(workbook: Workbook) = {
    val sheet = workbook.getSheetAt(0)
    parseSheet(sheet)
  }

  def parseSheet(sheet: Sheet) = {
    import scala.collection.JavaConversions._
    val res = for { row <- sheet } yield {
      parseRow(row)
    }
    res
  }

  def parseRow(row: Row) = {
    val cell = row.getCell(0)
    if (cell.getCellType == poi.Cell.CELL_TYPE_NUMERIC)
      cell.getNumericCellValue()
    else
      0.0
  }
}