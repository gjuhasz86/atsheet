package com.gjuhasz.atsheet

case class NoAnnotationFound(msg: String) extends RuntimeException(msg)