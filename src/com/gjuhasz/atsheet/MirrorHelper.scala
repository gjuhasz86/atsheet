package com.gjuhasz.atsheet
import scala.reflect.runtime.{ universe => ru }

object MirrorHelper {

  def instantiate[ClassT: ru.TypeTag](constructorSymbol: ru.Symbol, params: Any*): ClassT = {
    val runtimeMirror = ru.runtimeMirror(ru.typeOf[ClassT].getClass.getClassLoader)
    val thingMirror = runtimeMirror.reflectClass(ru.typeOf[ClassT].typeSymbol.asClass)
    val thingCostrunctorMirror = thingMirror.reflectConstructor(constructorSymbol.asMethod)
    val thingInstance = thingCostrunctorMirror(params: _*).asInstanceOf[ClassT]
    thingInstance
  }

  def instantiateByAnnotation[ClassT: ru.TypeTag, AnnotationT: ru.TypeTag](params: Any*): ClassT = {
    val thingConstructorSymbol = getFirstConstructorByAnnotation[ClassT, AnnotationT]
    instantiate[ClassT](thingConstructorSymbol, params: _*)
  }

  def getConstructorsByAnnotation[ClassT: ru.TypeTag, AnnotationT: ru.TypeTag]: List[ru.Symbol] = {
    val thingConstructorTerm = ru.typeOf[ClassT].decl(ru.nme.CONSTRUCTOR).asTerm
    thingConstructorTerm.alternatives.filter(constructor => isAnnotatedBy[AnnotationT](constructor))
  }

  def getFirstConstructorByAnnotation[ClassT: ru.TypeTag, AnnotationT: ru.TypeTag]: ru.Symbol = {
    getConstructorsByAnnotation[ClassT, AnnotationT].headOption
      .getOrElse(throw new NoAnnotationFound(s"Not found constructor on class [${ru.typeOf[ClassT]}] annotated with [${ru.typeOf[AnnotationT]}]"))
  }

  def getArgsWithAnnotationAndTargetType[AnnotationT: ru.TypeTag](symbol: ru.Symbol): List[(AnnotationT, ru.Type)] =
    for {
      paramList <- symbol.asMethod.paramLists
      param <- paramList
      annotation <- param.annotations if isTypeOf[AnnotationT](annotation)
      annotationInstance <- instantiateAnnotatation[AnnotationT](annotation)
    } yield (annotationInstance, param.typeSignature)

  private def instantiateAnnotatation[AnnotationT: ru.TypeTag](annotation: ru.Annotation): Option[AnnotationT] = {
    if (!(annotation.tree.tpe <:< ru.typeOf[AnnotationT])) {
      None
    } else {
      val args = annotation.scalaArgs.map(a => a.productElement(0).asInstanceOf[ru.Constant].value)
      val constructor = ru.typeOf[AnnotationT].decl(ru.nme.CONSTRUCTOR).alternatives.head
      val instance = instantiate[AnnotationT](constructor, args: _*)
      Option(instance)
    }
  }

  private def isAnnotatedBy[AnnotationT: ru.TypeTag](symbol: ru.Symbol): Boolean =
    symbol.annotations.exists(a => isTypeOf[AnnotationT](a))

  private def isTypeOf[AnnotationT: ru.TypeTag](annotation: ru.Annotation): Boolean =
    annotation.tree.tpe <:< ru.typeOf[AnnotationT]

}