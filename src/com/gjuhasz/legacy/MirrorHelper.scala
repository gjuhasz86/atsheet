package com.gjuhasz.legacy

import scala.reflect.runtime.{ universe => ru }

object MirrorHelper {
  def getAnnotatedMembers[ClassT: ru.TypeTag, AnnotationT: ru.TypeTag]: List[ru.Symbol] = {
    val allMembers = ru.typeOf[ClassT].members

    allMembers.filter { member =>
      member.annotations.exists { x => x.tree.tpe == ru.typeOf[AnnotationT] }
    }.toList
  }

  val isVal: (ru.Symbol) => (Boolean) =
    (sym: ru.Symbol) => sym.isTerm && sym.asTerm.isVal

  private def isTypeOf[AnnotationT: ru.TypeTag](annotation: ru.Annotation): Boolean = {
    annotation.tree.tpe == ru.typeOf[AnnotationT]
  }

  def getAnnotationMap[ClassT: ru.TypeTag, AnnotationT: ru.TypeTag] =
    for {
      member <- getAnnotatedMembers[ClassT, AnnotationT]
      annotation <- member.annotations if isTypeOf[AnnotationT](annotation)
    } yield (annotation, member)

  def getAnnotatedValueMap[ClassT: ru.TypeTag, AnnotationT: ru.TypeTag] =
    getAnnotationMap[ClassT, AnnotationT].filter { case (annotation, member) => isVal(member) }

  def instantiate[ClassT: ru.TypeTag](constructorSymbol: ru.Symbol, params: Any*): ClassT = {
    val runtimeMirror = ru.runtimeMirror(ru.typeOf[ClassT].getClass.getClassLoader)
    val thingMirror = runtimeMirror.reflectClass(ru.typeOf[ClassT].typeSymbol.asClass)
    val thingCostrunctorMirror = thingMirror.reflectConstructor(constructorSymbol.asMethod)
    val thingInstance = thingCostrunctorMirror(params: _*).asInstanceOf[ClassT]
    thingInstance

  }

  def instantiateByAnnotation[ClassT: ru.TypeTag, AnnotationT: ru.TypeTag](params: Any*): ClassT = {
    val thingConstructorTerm = ru.typeOf[ClassT].decl(ru.nme.CONSTRUCTOR).asTerm
    val thingConstructorSymbols = thingConstructorTerm.alternatives.find(x => x.annotations.exists(a => isTypeOf[AnnotationT](a)))
    val thingConstructorSymbol = thingConstructorSymbols.get
    instantiate[ClassT](thingConstructorSymbol, params: _*)
  }

  def instantiateFirst[ClassT: ru.TypeTag](params: Any*): ClassT = {
    val thingConstructorTerm = ru.typeOf[ClassT].decl(ru.nme.CONSTRUCTOR).asTerm
    val thingConstructorSymbols = thingConstructorTerm.alternatives.map(_.asMethod)
    val thingConstructorSymbol = thingConstructorSymbols.head
    instantiate[ClassT](thingConstructorSymbol, params: _*)
  }
}