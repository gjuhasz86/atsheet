package com.gjuhasz.legacy

import java.io.InputStream

import scala.collection.JavaConversions.iterableAsScalaIterable
import scala.reflect.ClassTag
import scala.reflect.runtime.universe

import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook

import scala.reflect.runtime.{ universe => ru }

class Parser(is: InputStream) {

  def parseTo[TargetT: ru.TypeTag](getTemplate: () => TargetT)(implicit t: ClassTag[TargetT]): List[TargetT] = {
    val list = parse()
    val memberSymToSet = MirrorHelper.getAnnotatedValueMap[TargetT, SheetField].head._2
    val memberTermToSet = memberSymToSet.asTerm

    val mirror = ru.runtimeMirror(ru.typeOf[TargetT].getClass.getClassLoader)

    list.map(cellValue => {
      val template = getTemplate()

      println("value: " + cellValue)
      println("before: " + template)
      val instanceMirror = mirror.reflect(template)(t)
      val field = instanceMirror.reflectField(memberTermToSet)
      field.set(cellValue.toInt)

      println("after: " + template)
      template
    }).toList
  }

  def parse() = {
    val workbook = new HSSFWorkbook(is)
    parseWorkBook(workbook)
  }

  def parseWorkBook(workbook: Workbook) = {
    val sheet = workbook.getSheetAt(0)
    parseSheet(sheet)
  }

  def parseSheet(sheet: Sheet) = {
    import scala.collection.JavaConversions._
    val res = for { row <- sheet } yield {
      parseRow(row)
    }
    res
  }

  def parseRow(row: Row) = {
    val cell = row.getCell(0)
    if (cell.getCellType == Cell.CELL_TYPE_NUMERIC)
      cell.getNumericCellValue()
    else
      0.0
  }
}