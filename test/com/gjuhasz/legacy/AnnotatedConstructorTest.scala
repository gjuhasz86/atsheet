package com.gjuhasz.legacy

import scala.annotation.StaticAnnotation
import scala.reflect.runtime.universe
import scala.reflect.runtime.{ universe => ru }
import org.junit.Assert
import org.junit.Test

case class Reflect(p: Int) extends StaticAnnotation

case class Thing(
  @Reflect(p = 10) val t1: Int,
  @Reflect(p = 20) val t2: Int)

case class Thing2(
  val t1: Int,
  val t2: String,
  val t3: Double)

case class Thing3(
  val t1: String) {

  @Reflect(p = 0)
  def this(t2: Int) = {
    this(t2.toString)
  }

  def this(t3: Int, t4: Int, t5: String) = {
    this((t3 + t4).toString + t5)
  }
  def this(t6: Thing2) = {
    this(t6.toString)
  }

}

class AnnotatedConstructorTest {

  @Test
  def testInstantiate: Unit = {
    val thing: Thing = MirrorHelper.instantiateFirst[Thing](1, 2)
    Assert.assertEquals(Thing(1, 2), thing)
  }

  @Test
  def testInstantiate2: Unit = {
    val thing: Thing2 = MirrorHelper.instantiateFirst[Thing2](1, "abc", 3.0)
    Assert.assertEquals(Thing2(1, "abc", 3.0), thing)
  }

  @Test
  def testInstantiate3first: Unit = {
    val thing: Thing3 = MirrorHelper.instantiateFirst[Thing3]("abc")
    Assert.assertEquals(Thing3("abc"), thing)
  }

  @Test
  def testInstantiate3byAnnotation: Unit = {
    val thing: Thing3 = MirrorHelper.instantiateByAnnotation[Thing3, Reflect](10)
    Assert.assertEquals(new Thing3(10), thing)
  }

  @Test
  def testCallConstructor: Unit = {
    val allMembers = ru.typeOf[Thing].members

    val constructors = allMembers.filter(x => x.isConstructor)

    val c = constructors.head

    println(c)
    println(c.asMethod.paramLists)
    val params = c.asMethod.paramLists.head

    val mapped = params.map(p => {
      println(p)
      println(p.annotations)
      val args = p.annotations.head.tree.children.tail
      println(args)
      val element = args.head.productElement(0)
      element.asInstanceOf[ru.Constant].value
    })
    println(mapped)
    println(params)
    println

    val runtimeMirror = ru.runtimeMirror(ru.typeOf[Thing].getClass.getClassLoader)
    val thingMirror = runtimeMirror.reflectClass(ru.typeOf[Thing].typeSymbol.asClass)
    val thingConstructorSymbol = ru.typeOf[Thing].decl(ru.nme.CONSTRUCTOR).asMethod
    val thingCostrunctorMirror = thingMirror.reflectConstructor(thingConstructorSymbol)
    val thingInstance = thingCostrunctorMirror(mapped: _*).asInstanceOf[Thing]
    println(thingInstance)

    Assert.assertEquals(Thing(10, 20), thingInstance)
  }
}