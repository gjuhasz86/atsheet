package com.gjuhasz.legacy

import org.junit.Assert
import org.junit.Test
import scala.io.Source
import java.io.FileInputStream
import scala.reflect.ClassTag

class TestSimpleSheet {

  @Test
  def shouldParseNumbers(): Unit = {
    val fis = new FileInputStream("testdata/numbersOnly.xls")
    val parser = new Parser(fis)
    val actual = parser.parse().toList
    Assert.assertEquals(List(1.0, 2.0, 3.0, 4.0, 5.0, 6.0), actual)
  }

  // changing fields on case classes reflectively won't change equals and toString results
  case class DataRow(n: Int) {
    @SheetField
    val number: Int = n

    override def toString = s"DataRow(${number.toString})"
    override def equals(other: Any) = number.equals(other.asInstanceOf[DataRow].number)
  }

  @Test
  def shouldParseNumbersIntoObjects: Unit = {

    val fis = new FileInputStream("testdata/numbersOnly.xls")
    val parser = new Parser(fis)
    def templateFactory() = new DataRow(0)

    val actual = parser.parseTo[DataRow](templateFactory)

    val expected = List(DataRow(1), DataRow(2), DataRow(3), DataRow(4), DataRow(5), DataRow(6))

    Assert.assertEquals(expected, actual)
  }
}