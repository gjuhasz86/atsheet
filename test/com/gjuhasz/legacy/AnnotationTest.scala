package com.gjuhasz.legacy

import scala.annotation.StaticAnnotation
import scala.reflect.runtime.universe

import org.junit.Assert
import org.junit.Test

import scala.reflect.runtime.{ universe => ru }

class AnnotationTest {

  case class SimpleAnnotation() extends StaticAnnotation
  case class AnotherSimpleAnnotation() extends StaticAnnotation

  case class SimpleAnnotated() {
    @SimpleAnnotation
    val member1: Int = 1

    @SimpleAnnotation
    def member2: Int = 2

    val member3: String = "3"

    def member4: Int = 4

    @SimpleAnnotation @AnotherSimpleAnnotation
    val member5: Int = 5

  }

  @Test
  def testGetAnnotatedMembers: Unit = {
    val members = MirrorHelper.getAnnotatedMembers[SimpleAnnotated, SimpleAnnotation]
    val actual = members.map(_.name).toSet
    val expected = Set("member5 ", "member2", "member1 ").map(ru.TermName(_))

    Assert.assertEquals(expected, actual)
  }

  @Test
  def testGetAnnotatedVals: Unit = {
    val valMembers =
      MirrorHelper
        .getAnnotatedMembers[SimpleAnnotated, SimpleAnnotation]
        .filter(MirrorHelper.isVal(_))

    val actual = valMembers.map(_.name).toSet
    val expected = Set("member5 ", "member1 ").map(ru.TermName(_))

    Assert.assertEquals(expected, actual)
  }

  @Test
  def testSetAnnotatedValMembers = {
    val valMembers =
      MirrorHelper
        .getAnnotatedMembers[SimpleAnnotated, SimpleAnnotation]
        .filter(MirrorHelper.isVal(_))

    val instance = new SimpleAnnotated
    Assert.assertEquals(1, instance.member1)
    Assert.assertEquals(5, instance.member5)

    val mirror = ru.runtimeMirror(instance.getClass.getClassLoader)
    val instanceMirror = mirror.reflect(instance)

    valMembers.foreach { member =>
      {
        val field = instanceMirror.reflectField(member.asTerm)
        field.set(10)
      }
    }

    Assert.assertEquals(10, instance.member1)
    Assert.assertEquals(10, instance.member5)

  }

  @Test
  def testGetAnnotationMap = {
    val resultMap = MirrorHelper.getAnnotationMap[SimpleAnnotated, SimpleAnnotation]
    val actual = resultMap.map { case (annotation, member) => (annotation.tree.tpe, member.name) }.toSet

    val expected = Set(
      (ru.typeOf[SimpleAnnotation], ru.TermName("member5 ")),
      (ru.typeOf[SimpleAnnotation], ru.TermName("member2")),
      (ru.typeOf[SimpleAnnotation], ru.TermName("member1 ")))

    Assert.assertEquals(expected, actual)
  }

  @Test
  def testGetAnnotationValueMap = {
    val resultMap = MirrorHelper.getAnnotatedValueMap[SimpleAnnotated, SimpleAnnotation]
    val actual = resultMap.map { case (annotation, member) => (annotation.tree.tpe, member.name) }.toSet

    val expected = Set(
      (ru.typeOf[SimpleAnnotation], ru.TermName("member5 ")),
      (ru.typeOf[SimpleAnnotation], ru.TermName("member1 ")))

    Assert.assertEquals(expected, actual)
  }
}