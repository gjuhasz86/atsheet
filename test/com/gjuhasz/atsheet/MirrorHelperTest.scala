package com.gjuhasz.atsheet

import scala.annotation.StaticAnnotation
import org.junit.Assert._
import org.junit.Test
import scala.reflect.runtime.universe.typeOf

case class Ann() extends StaticAnnotation
case class Par(param: Int) extends StaticAnnotation

case class AnnotatedCaseClass @Ann() (val param1: Int)

case class AnnotatedNonDefault(val param1: Int) {
  @Ann
  def this(param2: String) = this(param2.toInt)
}

case class NoAnnotation(val param1: Int)

case class EmptyConstructor @Ann() ()

case class SingleParams @Ann() (@Par(1) val param1: Int)

case class MultipleParams @Ann() (
  @Par(1) val param1: Int,
  val param2: Int,
  @Par(3) val param3: String)

case class DoublyAnnotatedParams @Ann() (
  @Par(1)@Par(2) val param1: Int,
  val param2: Int,
  @Par(3) val param3: String)

class MirrorHelperTest {

  @Test
  def shouldInstantiateAnnotatedCaseClass: Unit = {
    val symbol = MirrorHelper.getFirstConstructorByAnnotation[AnnotatedCaseClass, Ann]
    val actual = MirrorHelper.instantiate[AnnotatedCaseClass](symbol, 1)
    val expected = AnnotatedCaseClass(1)
    assertEquals(expected, actual)
  }

  @Test
  def shouldInstantiateAnnotatedNonDefault: Unit = {
    val symbol = MirrorHelper.getFirstConstructorByAnnotation[AnnotatedNonDefault, Ann]
    val actual = MirrorHelper.instantiate[AnnotatedNonDefault](symbol, "1")
    val expected = new AnnotatedNonDefault("1")
    assertEquals(expected, actual)
  }

  @Test(expected = classOf[NoAnnotationFound])
  def shouldThrowWhenNoAnnotationFound: Unit = {
    MirrorHelper.getFirstConstructorByAnnotation[NoAnnotation, Ann]
  }

  @Test
  def shouldReturnEmptyParameterMap: Unit = {
    val symbol = MirrorHelper.getFirstConstructorByAnnotation[EmptyConstructor, Ann]
    val actual = MirrorHelper.getArgsWithAnnotationAndTargetType[Par](symbol)
    val expected = List()
    assertEquals(expected, actual)
  }

  @Test
  def shouldReturnSingleParameterMap: Unit = {
    val symbol = MirrorHelper.getFirstConstructorByAnnotation[SingleParams, Ann]
    val actual = MirrorHelper.getArgsWithAnnotationAndTargetType[Par](symbol)
    val expected = List((Par(1), typeOf[Int]))
    assertEquals(expected, actual)
  }

  @Test
  def shouldReturnMultipleParameterMap: Unit = {
    val symbol = MirrorHelper.getFirstConstructorByAnnotation[MultipleParams, Ann]
    val actual = MirrorHelper.getArgsWithAnnotationAndTargetType[Par](symbol)
    val expected = List((Par(1), typeOf[Int]), (Par(3), typeOf[String]))
    assertEquals(expected, actual)
  }

  @Test
  def shouldReturnDoublyAnnotatedParameterTwice: Unit = {
    val symbol = MirrorHelper.getFirstConstructorByAnnotation[DoublyAnnotatedParams, Ann]
    val actual = MirrorHelper.getArgsWithAnnotationAndTargetType[Par](symbol)
    val expected = List((Par(1), typeOf[Int]), (Par(2), typeOf[Int]), (Par(3), typeOf[String]))
    assertEquals(expected, actual)
  }

}