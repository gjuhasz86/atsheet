package com.gjuhasz.atsheet

import java.io.FileInputStream
import org.junit.Assert
import org.junit.Test

case class NumericData @Cell(column = 0) (val n: Int)

class ParserTest {

  @Test
  def shouldParseNumbers(): Unit = {
    val fis = new FileInputStream("testdata/numbersOnly.xls")
    val parser = new Parser(fis)
    val actual = parser.parse().toList
    Assert.assertEquals(List(1.0, 2.0, 3.0, 4.0, 5.0, 6.0), actual)
  }
}