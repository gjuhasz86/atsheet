package com.gjuhasz.atsheet

import scala.annotation.StaticAnnotation
import org.junit.Test
import org.junit.Assert._

case class InjectValue[T](value: T) extends StaticAnnotation
case class Marker() extends StaticAnnotation

case class AnnotatedClass @Marker() (
  @InjectValue(42) val intArg: Int,
  @InjectValue("text") val stringArg: String)

class InstantiateByAnnotationExampleTest {

  @Test
  def testInstantiate: Unit = {
    val constructor = MirrorHelper.getFirstConstructorByAnnotation[AnnotatedClass, Marker]
    val argsMap = MirrorHelper.getArgsWithAnnotationAndTargetType[InjectValue[_]](constructor)
    val args = argsMap.map { case (annotation, _) => annotation.value }
    val actual = MirrorHelper.instantiate[AnnotatedClass](constructor, args: _*)
    val expected = AnnotatedClass(42, "text")
    assertEquals(expected, actual)
  }
}